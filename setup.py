import os, sys

print('-------- Installing Project Dependencies, this could take a while --------')
print('\nPlease wait...\n')

os.execv('pip install', ['Flask', 'flask_sqlalchemy', 'wtforms', 'numpy', 'sklearn'])

print('\n\n\n')
print('Completed.')



