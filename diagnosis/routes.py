
from flask import render_template, url_for, request, jsonify
from diagnosis import app
from diagnosis.models import Sympthon, Condition
from diagnosis.forms import DiagnoseForm
from diagnosis.helpers import peformDiagnosis, valuePrediction




@app.route('/test')
def hello():
	conditions = Condition.query.filter_by(name='Cancer').all()
	return render_template('index-old.html', conditions=conditions, header='Welcome to Diagnosis [Beta]')


@app.route('/step', methods=['POST'])
def step():
	id = request.form.get('condition')
	if not (id): return 'Nothing to test for <a href="../test">Go back</a>'
	condition = Condition.query.filter_by(id=id).first()
	sympthons = condition.sympthons
	count_sympthon = len(sympthons)
	return render_template('step.html', count_sympthon=count_sympthon, conditions= [condition], sympthons=sympthons, header='Diagnosis for ' + condition.name)


@app.route('/result', methods=['POST', 'GET'])
def diagnose():
	form = DiagnoseForm(request.form)	
	result = peformDiagnosis(formData=request.form)
	return render_template('result.html', header='Diagnosis Result', result=result)


# @app.route('/sympthons', methods=['GET'])
# def sympthons():
# 	id = request.args.get('condition_id')
# 	c = Condition.query.filter_by(id=id).all()[0]
# 	print(len(c.sympthons))

# 	sympthons = []
# 	for index, sympthon in enumerate(c.sympthons):
# 		print (index)
# 		sympthons += [dict(desc=sympthon.__repr__())]

# 		return jsonify({"sympthons":sympthons})


@app.route('/', methods=['GET'])
@app.route('/prediction', methods=['GET'])
def ai():
	return render_template('index.html', header='Cancer Tumor Prediction')


@app.route('/predict', methods=['POST'])
def predict():
    if request.method == 'POST':
        to_predict_list = request.form.to_dict()

		# Calculate correct arguements provided
        occour = [ int(float(enough)) >= 2 for enough in list(request.form.to_dict().values()) ]
        occurs = []
        for test in occour:
        	if test: occurs.append(test)
		
		# Check whether there is enough argurments		
        if len(occurs) <= 1:
        	prediction = 'Indeterminable'
        	return render_template("result-ai.html", header='Predition Result', prediction=prediction)


        to_predict_list = list(to_predict_list.values())
        to_predict_list = list(map(float, to_predict_list))
        result = valuePrediction(to_predict_list)
        
        if int(result)==1:
            prediction='Malignant'
        else:
            prediction='Benign'
            
    return render_template("result-ai.html", header='Prediction Result', prediction=prediction)