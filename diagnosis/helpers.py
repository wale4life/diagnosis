from diagnosis.models import Condition, Sympthon
import numpy as np
import pickle
import os


# Perform Diagnosis based on provided form data
def peformDiagnosis(formData):

	# Parse form data into a list
	data = formData.to_dict(flat=False)
	conditions = list(data.values())

	# Confirm the user has answer the questions correctly
	occurances = [ len(item) for item in conditions ]
	if(len(set(occurances)) == 1 and len(conditions) > 1 ):
		return 'Indeterminable'

	## Determine which condition appear most from the sympthon
	# maxLength = max([ len(item) for item in conditions ])
	# for item in conditions:
	# 	if(len(item) == maxLength): 
	# 		nigga = item[0]

	
	if not occurances:
		return 'Indeterminable'

	nigga = max(conditions, key= lambda item: len(item))

	# print(data)
	if(len(nigga) == 1):
		return 'Indeterminable'

	c = Condition.query.filter_by(name=nigga[0]).first()
	x = len(c.sympthons)
	nigga_occurances = len(nigga)
	
	nigga_percentage = (float(nigga_occurances)/float(x)) * 100
	if (nigga_percentage >= 87): nigga_percentage -= 12 

	return  [ nigga[0], round(nigga_percentage, 2) ]


def performDiagnosisOld(formData):

	# Parse form data into a list
	data = formData.to_dict(flat=False)
	conditions = list(data.values())[3:]

	# print(data);
	# Confirm the user has answer the questions correctly
	occurances = [ len(item) for item in conditions ]
	if(len(set(occurances)) == 1 and len(conditions) > 1 ):
		return 'Indeterminable'

	## Determine which condition appear most from the sympthon
	# maxLength = max([ len(item) for item in conditions ])
	# for item in conditions:
	# 	if(len(item) == maxLength): 
	# 		nigga = item[0]

	print(data)
	nigga = max(conditions, key= lambda item: len(item))


	nigga_occurances = len(nigga[1])
	nigga_percentage = nigga_occurances + (nigga_occurances/Sympthon.query.count()) * 100

	return  [ nigga[0], nigga_percentage ]


def preprocessData():

	# Load Dataset
	df = np.read_csv("ai-model/data.csv")

	# Fill in missing values
	col_name = df.columns
	for c in col_name:
		df[c] = df[c].replace("?", np.NaN)

	df = df.apply(lambda x:x.fillna(x.value_counts().index[0]))



def valuePrediction(to_predict_list):
	to_predict = np.array(to_predict_list).reshape(1,30)
	loaded_model = pickle.load(open("ai-model/model2.pkl","rb"))
	result = loaded_model.predict(to_predict)
	return result[0]