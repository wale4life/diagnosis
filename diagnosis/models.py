from diagnosis import db


class Condition(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(), nullable=False)
	sympthons = db.relationship('Sympthon', lazy=True)

	def __repr__(self):
		return "Condition - %s" % self.name



class Sympthon(db.Model): 
	id = db.Column(db.Integer, primary_key=True)
	condition_id = db.Column(db.Integer, db.ForeignKey('condition.id'), nullable=False)
	desc = db.Column(db.String(), nullable=False)
	photo = db.Column(db.String(), nullable=True)

	def __repr__(self):
		return 'Sympthon - %s' % self.desc


