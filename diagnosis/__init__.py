from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

app.config['SECRET_KEY'] = '83228e5d73bfae383765a90c61f70f837744de2691e8a3e0b6ea1d0607084a60'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'

db = SQLAlchemy(app)

from diagnosis import routes